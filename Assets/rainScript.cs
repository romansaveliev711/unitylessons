﻿using UnityEngine;
using System.Collections;

public class rainScript : MonoBehaviour {
	public Transform rainPrefab;
	public float RandomX;
	public float RandomZ;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		RandomX = Random.Range (0f, 100f);
		RandomZ = Random.Range (0f, 100f);
		Instantiate (rainPrefab, new Vector3 (RandomX, 10, RandomZ), Quaternion.identity);
	}
}
